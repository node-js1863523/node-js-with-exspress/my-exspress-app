var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
    const products = [
        { id : 1, name : 'Products 1', price : 10.20},
        { id : 2, name : 'Products 2', price : 12.30},
        { id : 3, name : 'Products 3', price : 15.90}
    ];
    res.json(products);
});

router.get('/:id', function(req, res){
    const productid = req.params.id;
    const products = [
        { id : 1, name : 'Products 1', price : 10.20},
        { id : 2, name : 'Products 2', price : 12.30},
        { id : 3, name : 'Products 3', price : 15.90}
    ];
    const product = products.find(product => product.id ==productid);
    if( product ){
        res.json(product);
    }
    else{
        res.sendStatus(404);
    }
});

router.post('/', (req, res) => {
    const newProduct = req.body;
    newProduct.id = Date.now(); // Simple ID generation// Here you would normally save the product to the database
    res.status(201).json(newProduct);
});

module.exports = router;